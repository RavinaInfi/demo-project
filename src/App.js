import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

import Login from './Components/users/login/login';
import Signup from './Components/users/signup/signup';
import ErrorPage from './Components/users/homepage/errorPage';
import IsAuthenticated from './Auth';
import GoogleLogin from './Components/users/login/googleLogin';
import Layout from './Components/Layout/Layout';
import Header from './Components/Layout/Header';
import Footer from './Components/Layout/Footer';
import Homepage from './Components/users/homepage/homepage';
import UserMoviepage from './Components/movies/user/userMoviePage';

export default function App() {

  const isAuthenticated = localStorage.getItem("isAuthenticated");
  if (isAuthenticated) {
    return (
      <>
        <Router>
          <div class="wrapper">
          <Header />
          <div class="content-wrapper">
            <Layout />
          </div>
          <Footer />
          </div>
        </Router>
      </>
    )
  }else{
    return (
     <>
        <Router>
          <Routes>
            <Route path= '/home' element= {<Homepage/>} />

            <Route path = '/' element = {< Homepage />} />

            <Route path='/user-movie-page' element={<UserMoviepage />} />

            <Route path='/login' element={<IsAuthenticated/>}>
              <Route path='/login' element={<Login />} />
            </Route>
            <Route path='/create-account' element={<IsAuthenticated/>}>
              <Route path='/create-account' element={ <Signup />} />
            </Route>
            <Route path='/google-auth-login' element={<GoogleLogin/>}/>
            <Route path='*' element={<ErrorPage />} />
          </Routes>
        </Router>
      </>
    );
  }
}
