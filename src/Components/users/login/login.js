import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router";
import Swal2 from "sweetalert2";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
require('dotenv').config();


const Login = () => {
    let navigate = useNavigate();

    const [user, setUser] = useState({
        email: "",
        password: ""
    })

    const [passwordShown, setPasswordShown] = useState(false);

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    const handleChange = event => {
        const { name, value } = event.target
        setUser({
            ...user,
            [name]: value
        })
    }

    const login = (e) => {
        let { email, password } = user;
        if (email && password) {
            axios.post('/login', user)
            .then((res)=>{
                Swal2.fire({
                    icon : "success",
                    title : "Logged in Successfully."
                })
                return res
            })
            .then((res)=>{
                localStorage.setItem("isAuthenticated", "true");
                localStorage.setItem("role", res.data.role);
                navigate('/');
            })
            .catch ((err)=>{
                Swal2.fire({
                    icon : "error",
                    title : err.response.data.message
                })
            }) 
        }else{
            Swal2.fire({
                icon : "error",
                title : "Invalid input"
            })
        }
    }

    const validationSchema = Yup.object().shape({

        email: Yup.string()
            .required('Email is required')
            .email('Email is invalid')
            .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
            
        password: Yup.string()
            .required('Password is required')
            .min(6, 'Password must be at least 6 characters')
    });
    const formOptions = { resolver: yupResolver(validationSchema) };
    
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    return (
        <>
            <body class="hold-transition login-page">
            <div class="login-box">
            <div class="login-logo">
                <a href="http://localhost:3006" style={{ textDecoration: 'none' }} ><b>Movie Rental System</b></a>
            </div>
            {/* <!-- /.login-logo --> */}
            <div class="card">
                <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="../../index3.html" method="post">
                    <div class="input-group mb-3">
                        <input 
                            type="email" 
                            placeholder="Email"
                            name="email" 
                            class="form-control" 
                            {...register('email')} 
                            className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                            value={user.email}
                            onChange={handleChange}
                        />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">Please enter a correct email id.</div>
                    </div>


                    <div class="input-group mb-3">
                        <input 
                            class="form-control" 
                            placeholder="Password"
                            type={passwordShown ? "text" : "password"} 
                            name="password" 
                            {...register('password')} 
                            className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                            value={user.password}
                            onChange={handleChange}
                        />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">Please enter your password!</div>
                    </div>

                    <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                        <input 
                            type="checkbox" 
                            id="remember"
                            class="icheck-input" 
                            name="remember" 
                            value="true" 
                            onClick={togglePasswordVisiblity}
                        />
                        <label for="remember">
                            View Password
                        </label>
                        </div>
                    </div>

                    {/* <!-- /.col --> */}
                    <div class="col-4">
                        <button 
                            type="submit" 
                            class="btn btn-primary btn-block"
                            onClick = { handleSubmit(login) }
                            >Sign In
                        </button>
                    </div>
                    {/* <!-- /.col --> */}
                    </div>
                </form>

                <div class="social-auth-links text-center mb-3">
                    <p>- OR -</p>
                    {/* <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
                    </a> */}
                    <a href="http://localhost:3040/auth/google" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
                    </a>
                </div>
                {/* <!-- /.social-auth-links --> */}

                {/* <p class="mb-1">
                    <a href="forgot-password.html" style={{ textDecoration: 'none' }}>I forgot my password</a>
                </p> */}
                <p class="mb-0">
                    <a href="/create-account" class="text-center" style={{ textDecoration: 'none' }}>Register a new membership</a>
                </p>
                </div>
                {/* <!-- /.login-card-body --> */}
            </div>
            </div>
            </body>
        </>
    )
}

export default Login;