import React from "react";

const Homepage = () =>{

    const isAuthenticated = localStorage.getItem("isAuthenticated");
    const role = localStorage.getItem('role');
  
    if(role != null && isAuthenticated != null){
      if (isAuthenticated && role.toLowerCase() === 'admin') {
        return(
          <div class="wrapper">
            {/* <!-- Navbar --> */}
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              {/* <!-- Left navbar links --> */}
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                  <a href="/" class="nav-link">Home</a>
                </li>
              </ul>

              {/* <!-- Right navbar links --> */}
              <ul class="navbar-nav ml-auto">
                {/* <!-- Navbar Search --> */}
                <li class="nav-item">
                  <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                    <i class="fas fa-search"></i>
                  </a>
                  <div class="navbar-search-block" id="main-header-search">
                    <form class="form-inline">
                      <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                          </button>
                          <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                            <i class="fas fa-times"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                  </a>
                </li>
              </ul>
            </nav>
            {/* <!-- /.navbar --> */}
            
            {/* <!-- Main Sidebar Container --> */}
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
              {/* <!-- Brand Logo --> */}
              <a href="/" class="brand-link" style={{ 'textDecoration': 'none' }}>
                <img src='./dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
                <span class="brand-text font-weight-light">Movie Rental Sys</span>
              </a> 

              {/* <!-- Sidebar --> */}
              <div class="sidebar">
                {/* <!-- Sidebar user (optional) -->  */}
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <img src="dist/img/profile.png" class="img-circle elevation-2" alt="User Image"/>
                  </div>
                  <div class="info">
                    <a href="/profile" class="d-block" style={{ 'textDecoration': 'none' }}>Profile</a>
                  </div>
                </div>

                {/* <!-- SidebarSearch Form --> */}
                <div class="form-inline">
                  <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                    <div class="input-group-append">
                      <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                      </button>
                    </div>
                  </div>
                </div>

                {/* <!-- Sidebar Menu --> */}
                <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    {/* Dashboard */}
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                          Dashboard
                        </p>
                      </a>
                    </li>

                    {/* Movies */}
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                          Movies
                          <i class="fas fa-angle-left right"></i>
                          <span class="badge badge-info right"></span>
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="/movies-add-movie" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add Movie</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/admin-movie-page" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Admin Movie Page</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/user-movie-page" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>User Movie Page</p>
                          </a>
                        </li>
                      </ul>
                    </li>

                    {/* Users */}
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                          Users
                          <i class="right fas fa-angle-left"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="/users" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>All users</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/add-user" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Add user</p>
                          </a>
                        </li>
                      </ul>
                    </li>

                    {/* Lables */}
                    <li class="nav-header">LABELS</li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-warning"></i>
                        <p>Warning</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Informational</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/logout" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Logout</p>
                      </a>
                    </li>
                  </ul>
                </nav>
                {/* <!-- /.sidebar-menu --> */}
              </div>
              {/* <!-- /.sidebar --> */}
            </aside>
            <div class="content-wrapper">
              {/* <!-- Content Header (Page header) --> */}
              <div class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1 class="m-0">Dashboard</h1>
                    </div>
                    {/* <!-- /.col --> */}
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                      </ol>
                    </div>
                    {/* <!-- /.col --> */}
                  </div>
                  {/* <!-- /.row --> */}
                </div>
              {/* <!-- Main content --> */}
              <section class="content">
                <div class="container-fluid">
                  
                  {/* <!-- Small boxes (Stat box) --> */}
                  <div class="row">
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h3>150</h3>

                          <p>New Orders</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-success">
                        <div class="inner">
                          <h3>53<sup style={{"font-size": "20px"}}>%</sup></h3>

                          <p>Bounce Rate</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-warning">
                        <div class="inner">
                          <h3>44</h3>

                          <p>User Registrations</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-danger">
                        <div class="inner">
                          <h3>65</h3>

                          <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                  </div>
                  
                  {/* <!-- Main row --> */}
                  <div class="row">
                    
                    {/* <!-- Left col --> */}
                    <section class="col-lg-7 connectedSortable">
                      
                      {/* <!-- TO DO List --> */}
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            To Do List
                          </h3>

                          <div class="card-tools">
                            <ul class="pagination pagination-sm">
                              <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                              <li class="page-item"><a href="#" class="page-link">1</a></li>
                              <li class="page-item"><a href="#" class="page-link">2</a></li>
                              <li class="page-item"><a href="#" class="page-link">3</a></li>
                              <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                            </ul>
                          </div>
                        </div>
                        
                        {/* <!-- /.card-header --> */}
                        <div class="card-body">
                          <ul class="todo-list" data-widget="todo-list">
                            <li>
                              
                              {/* <!-- drag handle --> */}
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              
                              {/* <!-- checkbox --> */}
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo1" id="todoCheck1"/>
                                <label for="todoCheck1"></label>
                              </div>
                              
                              {/* <!-- todo text --> */}
                              <span class="text">Design a nice theme</span>
                              
                              {/* <!-- Emphasis label --> */}
                              <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                              
                              {/* <!-- General tools such as edit or delete--> */}
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo2" id="todoCheck2" checked/>
                                <label for="todoCheck2"></label>
                              </div>
                              <span class="text">Make the theme responsive</span>
                              <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo3" id="todoCheck3"/>
                                <label for="todoCheck3"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo4" id="todoCheck4"/>
                                <label for="todoCheck4"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo5" id="todoCheck5"/>
                                <label for="todoCheck5"></label>
                              </div>
                              <span class="text">Check your messages and notifications</span>
                              <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo6" id="todoCheck6"/>
                                <label for="todoCheck6"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-secondary"><i class="far fa-clock"></i> 1 month</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                          </ul>
                        </div>
                        {/* <!-- /.card-body --> */}
                        <div class="card-footer clearfix">
                          <button type="button" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Add item</button>
                        </div>
                      </div>
                      {/* <!-- /.card --> */}
                    </section>

                  </div>
                  {/* <!-- /.row (main row) --> */}
                </div>
                {/* <!-- /.container-fluid --> */}
              </section>
                {/* <!-- /.container-fluid --> */}
              </div>
            </div> 
          </div>
        )
      }else{
        return(
          <div class="wrapper">
            {/* <!-- Navbar --> */}
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              {/* <!-- Left navbar links --> */}
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                  <a href="/" class="nav-link">Home</a>
                </li>
              </ul>

              {/* <!-- Right navbar links --> */}
              <ul class="navbar-nav ml-auto">
                {/* <!-- Navbar Search --> */}
                <li class="nav-item">
                  <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                    <i class="fas fa-search"></i>
                  </a>
                  <div class="navbar-search-block" id="main-header-search">
                    <form class="form-inline">
                      <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                          </button>
                          <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                            <i class="fas fa-times"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                  </a>
                </li>
              </ul>
            </nav>
          {/* <!-- /.navbar --> */}
          
            {/* <!-- Main Sidebar Container --> */}
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
              {/* <!-- Brand Logo --> */}
              <a href="/" class="brand-link" style={{ 'textDecoration': 'none' }}>
                <img src='./dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
                <span class="brand-text font-weight-light">Movie Rental Sys</span>
              </a> 

              {/* <!-- Sidebar --> */}
              <div class="sidebar">
                {/* <!-- Sidebar user (optional) -->  */}
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <img src="dist/img/profile.png" class="img-circle elevation-2" alt="User Image"/>
                  </div>
                  <div class="info">
                    <a href="/profile" class="d-block" style={{ 'textDecoration': 'none' }}>Profile</a>
                  </div>
                </div>

                {/* <!-- SidebarSearch Form --> */}
                <div class="form-inline">
                  <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                    <div class="input-group-append">
                      <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                      </button>
                    </div>
                  </div>
                </div>

                {/* <!-- Sidebar Menu --> */}
                <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    {/* Dashboard */}
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                          Dashboard
                        </p>
                      </a>
                    </li>

                    {/* Movies */}
                    <li class="nav-item">
                      <a href="/user-movie-page" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                          Movies
                          {/* <i class="fas fa-angle-left right"></i> */}
                          <span class="badge badge-info right"></span>
                        </p>
                      </a>
                    </li>
                    {/* Lables */}
                    <li class="nav-header">LABELS</li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-warning"></i>
                        <p>Warning</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Informational</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/logout" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Logout</p>
                      </a>
                    </li>
                  </ul>
                </nav>
                {/* <!-- /.sidebar-menu --> */}
              </div>
              {/* <!-- /.sidebar --> */}
            </aside>
            <div class="content-wrapper">
              {/* <!-- Content Header (Page header) --> */}
              <div class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1 class="m-0">Dashboard</h1>
                    </div>
                    {/* <!-- /.col --> */}
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                      </ol>
                    </div>
                    {/* <!-- /.col --> */}
                  </div>
                  {/* <!-- /.row --> */}
                </div>
              {/* <!-- Main content --> */}
              <section class="content">
                <div class="container-fluid">
                  
                  {/* <!-- Small boxes (Stat box) --> */}
                  <div class="row">
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h3>150</h3>

                          <p>New Orders</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-success">
                        <div class="inner">
                          <h3>53<sup style={{"font-size": "20px"}}>%</sup></h3>

                          <p>Bounce Rate</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-warning">
                        <div class="inner">
                          <h3>44</h3>

                          <p>User Registrations</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-danger">
                        <div class="inner">
                          <h3>65</h3>

                          <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                  </div>
                  
                  {/* <!-- Main row --> */}
                  <div class="row">
                    
                    {/* <!-- Left col --> */}
                    <section class="col-lg-7 connectedSortable">
                      
                      {/* <!-- TO DO List --> */}
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            To Do List
                          </h3>

                          <div class="card-tools">
                            <ul class="pagination pagination-sm">
                              <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                              <li class="page-item"><a href="#" class="page-link">1</a></li>
                              <li class="page-item"><a href="#" class="page-link">2</a></li>
                              <li class="page-item"><a href="#" class="page-link">3</a></li>
                              <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                            </ul>
                          </div>
                        </div>
                        
                        {/* <!-- /.card-header --> */}
                        <div class="card-body">
                          <ul class="todo-list" data-widget="todo-list">
                            <li>
                              
                              {/* <!-- drag handle --> */}
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              
                              {/* <!-- checkbox --> */}
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo1" id="todoCheck1"/>
                                <label for="todoCheck1"></label>
                              </div>
                              
                              {/* <!-- todo text --> */}
                              <span class="text">Design a nice theme</span>
                              
                              {/* <!-- Emphasis label --> */}
                              <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                              
                              {/* <!-- General tools such as edit or delete--> */}
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo2" id="todoCheck2" checked/>
                                <label for="todoCheck2"></label>
                              </div>
                              <span class="text">Make the theme responsive</span>
                              <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo3" id="todoCheck3"/>
                                <label for="todoCheck3"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo4" id="todoCheck4"/>
                                <label for="todoCheck4"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo5" id="todoCheck5"/>
                                <label for="todoCheck5"></label>
                              </div>
                              <span class="text">Check your messages and notifications</span>
                              <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo6" id="todoCheck6"/>
                                <label for="todoCheck6"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-secondary"><i class="far fa-clock"></i> 1 month</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                          </ul>
                        </div>
                        {/* <!-- /.card-body --> */}
                        <div class="card-footer clearfix">
                          <button type="button" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Add item</button>
                        </div>
                      </div>
                      {/* <!-- /.card --> */}
                    </section>

                  </div>
                  {/* <!-- /.row (main row) --> */}
                </div>
                {/* <!-- /.container-fluid --> */}
              </section>
                {/* <!-- /.container-fluid --> */}
              </div>
            </div> 
          </div>
        )
      }
    }else{
      return(
        <div class="wrapper">
            {/* <!-- Navbar --> */}
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
              {/* <!-- Left navbar links --> */}
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" data-widget="pushmenu" href="/" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                  <a href="/" class="nav-link">Home</a>
                </li>
              </ul>

              {/* <!-- Right navbar links --> */}
              <ul class="navbar-nav ml-auto">
                {/* <!-- Navbar Search --> */}
                <li class="nav-item">
                  <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                    <i class="fas fa-search"></i>
                  </a>
                  <div class="navbar-search-block" id="main-header-search">
                    <form class="form-inline">
                      <div class="input-group input-group-sm">
                        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                        <div class="input-group-append">
                          <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                          </button>
                          <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                            <i class="fas fa-times"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>

                <li class="nav-item">
                  <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                  </a>
                </li>
              </ul>
            </nav>
            {/* <!-- /.navbar --> */}
            
            {/* <!-- Main Sidebar Container --> */}
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
              {/* <!-- Brand Logo --> */}
              <a href="/" class="brand-link" style={{ 'textDecoration': 'none' }}>
                <img src='./dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
                <span class="brand-text font-weight-light">Movie Rental Sys</span>
              </a> 

              {/* <!-- Sidebar --> */}
              <div class="sidebar">
                {/* <!-- Sidebar user (optional) -->  */}
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <img src="dist/img/login.jpeg" class="img-circle elevation-2" alt="User Image"/>
                  </div>
                  <div class="info">
                    <a href="/login" class="d-block" style={{ 'textDecoration': 'none' }}>Login</a>
                  </div>
                </div>
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                  <div class="image">
                    <img src="dist/img/signUp.png" class="img-circle elevation-2" alt="User Image"/>
                  </div>
                  <div class="info">
                    <a href="/create-account" class="d-block" style={{ 'textDecoration': 'none' }}>Signup</a>
                  </div>
                </div>

                {/* <!-- SidebarSearch Form --> */}
                <div class="form-inline">
                  <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                    <div class="input-group-append">
                      <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                      </button>
                    </div>
                  </div>
                </div>

                {/* <!-- Sidebar Menu --> */}
                <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                    {/* Dashboard */}
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                          Dashboard
                        </p>
                      </a>
                    </li>

                    {/* Movies */}
                    <li class="nav-item">
                      <a href="/login" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                          Movies
                          {/* <i class="fas fa-angle-left right"></i> */}
                          <span class="badge badge-info right"></span>
                        </p>
                      </a>
                    </li>

                    {/* Lables */}
                    <li class="nav-header">LABELS</li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-warning"></i>
                        <p>Warning</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Informational</p>
                      </a>
                    </li>
                  </ul>
                </nav>
                {/* <!-- /.sidebar-menu --> */}
              </div>
              {/* <!-- /.sidebar --> */}
            </aside>
            <div class="content-wrapper">
              {/* <!-- Content Header (Page header) --> */}
              <div class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1 class="m-0">Dashboard</h1>
                    </div>
                    {/* <!-- /.col --> */}
                    <div class="col-sm-6">
                      <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                      </ol>
                    </div>
                    {/* <!-- /.col --> */}
                  </div>
                  {/* <!-- /.row --> */}
                </div>
              {/* <!-- Main content --> */}
              <section class="content">
                <div class="container-fluid">
                  
                  {/* <!-- Small boxes (Stat box) --> */}
                  <div class="row">
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-info">
                        <div class="inner">
                          <h3>150</h3>

                          <p>New Orders</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-success">
                        <div class="inner">
                          <h3>53<sup style={{"font-size": "20px"}}>%</sup></h3>

                          <p>Bounce Rate</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-warning">
                        <div class="inner">
                          <h3>44</h3>

                          <p>User Registrations</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                    <div class="col-lg-3 col-6">
                      
                      {/* <!-- small box --> */}
                      <div class="small-box bg-danger">
                        <div class="inner">
                          <h3>65</h3>

                          <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    
                    {/* <!-- ./col --> */}
                  </div>
                  
                  {/* <!-- Main row --> */}
                  <div class="row">
                    
                    {/* <!-- Left col --> */}
                    <section class="col-lg-7 connectedSortable">
                      
                      {/* <!-- TO DO List --> */}
                      <div class="card">
                        <div class="card-header">
                          <h3 class="card-title">
                            <i class="ion ion-clipboard mr-1"></i>
                            To Do List
                          </h3>

                          <div class="card-tools">
                            <ul class="pagination pagination-sm">
                              <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                              <li class="page-item"><a href="#" class="page-link">1</a></li>
                              <li class="page-item"><a href="#" class="page-link">2</a></li>
                              <li class="page-item"><a href="#" class="page-link">3</a></li>
                              <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                            </ul>
                          </div>
                        </div>
                        
                        {/* <!-- /.card-header --> */}
                        <div class="card-body">
                          <ul class="todo-list" data-widget="todo-list">
                            <li>
                              
                              {/* <!-- drag handle --> */}
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              
                              {/* <!-- checkbox --> */}
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo1" id="todoCheck1"/>
                                <label for="todoCheck1"></label>
                              </div>
                              
                              {/* <!-- todo text --> */}
                              <span class="text">Design a nice theme</span>
                              
                              {/* <!-- Emphasis label --> */}
                              <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                              
                              {/* <!-- General tools such as edit or delete--> */}
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo2" id="todoCheck2" checked/>
                                <label for="todoCheck2"></label>
                              </div>
                              <span class="text">Make the theme responsive</span>
                              <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo3" id="todoCheck3"/>
                                <label for="todoCheck3"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo4" id="todoCheck4"/>
                                <label for="todoCheck4"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo5" id="todoCheck5"/>
                                <label for="todoCheck5"></label>
                              </div>
                              <span class="text">Check your messages and notifications</span>
                              <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                            <li>
                              <span class="handle">
                                <i class="fas fa-ellipsis-v"></i>
                                <i class="fas fa-ellipsis-v"></i>
                              </span>
                              <div  class="icheck-primary d-inline ml-2">
                                <input type="checkbox" value="" name="todo6" id="todoCheck6"/>
                                <label for="todoCheck6"></label>
                              </div>
                              <span class="text">Let theme shine like a star</span>
                              <small class="badge badge-secondary"><i class="far fa-clock"></i> 1 month</small>
                              <div class="tools">
                                <i class="fas fa-edit"></i>
                                <i class="fas fa-trash-o"></i>
                              </div>
                            </li>
                          </ul>
                        </div>
                        {/* <!-- /.card-body --> */}
                        <div class="card-footer clearfix">
                          <button type="button" class="btn btn-primary float-right"><i class="fas fa-plus"></i> Add item</button>
                        </div>
                      </div>
                      {/* <!-- /.card --> */}
                    </section>

                  </div>
                  {/* <!-- /.row (main row) --> */}
                </div>
                {/* <!-- /.container-fluid --> */}
              </section>
                {/* <!-- /.container-fluid --> */}
              </div>
            </div> 
        </div>
      )
    }
    
}

export default Homepage;





// import React, {useState, useEffect} from "react";
// import { useNavigate } from "react-router";

// const Homepage = () =>{
//     let navigate = useNavigate();

//     return (
//         <>
//           <div className="App">
//               <div className="homepage">
//                   <h1 className="text-center text-warning" > ** You're Well Come To Home-Page **</h1>
//                   <button className="button" onClick={() => navigate('/admin-movie-page')} >Admin Movie Page</button>
//                   <button className="button" onClick={() => navigate('/user-movie-page')} >User Movie Page</button>
//               </div>
//           </div>
//         </>
//     )
// }

// export default Homepage;