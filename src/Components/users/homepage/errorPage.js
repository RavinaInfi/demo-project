import React from "react";

export default function ErrorPage(){
  const isAuthenticated = localStorage.getItem("isAuthenticated");
  if (isAuthenticated) {
    return (
      <>
        {/* <div class="content-wrapper"> */}
          {/* <!-- Content Header (Page header) --> */}
          <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>404 Error Page</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">404 Error Page</li>
                  </ol>
                </div>
              </div>
            </div>
            {/* <!-- /.container-fluid --> */}
          </section>

          {/* <!-- Main content --> */}
          <section class="content">
            <div class="error-page">
              <h2 class="headline text-warning"> 404</h2>

              <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

                <p>
                  We could not find the page you were looking for.
                  Meanwhile, you may <a href="../../index.html">return to dashboard</a> or try using the search form.
                </p>

                <form class="search-form">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search"/>

                    <div class="input-group-append">
                      <button type="submit" name="submit" class="btn btn-warning"><i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                  {/* <!-- /.input-group --> */}
                </form>
              </div>
              {/* <!-- /.error-content --> */}
            </div>
            {/* <!-- /.error-page --> */}
          </section>
          {/* <!-- /.content --> */}
        {/* </div> */}
        {/* <div class="container">
          <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
            <h1>404</h1>
            <h2>The page you are looking for doesn't exist.</h2>
            <a class="btn" href="http://localhost:3006">Back to home</a>
            <img src="assets/img/not-found.svg" class="img-fluid py-5" alt="Page Not Found"/>
          </section>
        </div> */}
      </>
      )
  }else{
    return (
      <>
        {/* <div class="content-wrapper"> */}
          {/* <!-- Content Header (Page header) --> */}
          <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>404 Error Page</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">404 Error Page</li>
                  </ol>
                </div>
              </div>
            </div>
            {/* <!-- /.container-fluid --> */}
          </section>

          {/* <!-- Main content --> */}
          <section class="content">
            <div class="error-page">
              <h2 class="headline text-warning"> 404</h2>

              <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>

                <p>
                  We could not find the page you were looking for.
                  Meanwhile, you may <a href="../../index.html">return to dashboard</a> or try using the search form.
                </p>

                <form class="search-form">
                  <div class="input-group">
                    <input type="text" name="search" class="form-control" placeholder="Search"/>

                    <div class="input-group-append">
                      <button type="submit" name="submit" class="btn btn-warning"><i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                  {/* <!-- /.input-group --> */}
                </form>
              </div>
              {/* <!-- /.error-content --> */}
            </div>
            {/* <!-- /.error-page --> */}
          </section>
          {/* <!-- /.content --> */}
        {/* </div> */}
        {/* <div class="container">
          <section class="section error-404 min-vh-100 d-flex flex-column align-items-center justify-content-center">
            <h2>Movie Rental System</h2>
            {/* <h2>WellCome</h2> */}
            {/*<h3>Please Login/ Create Account</h3>
            <a class="btn" href="http://localhost:3006/login">Login</a>
            <a class="btn" href="http://localhost:3006/create-account">Create Account</a>
            <img src="assets/img/not-found.svg" class="img-fluid py-5" alt="Page Not Found"/>
          </section>
        </div> */}
      </>
      )
  }
}