import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router";
import Swal2 from "sweetalert2";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function Signup(){
    let navigate = useNavigate();

    const [user, setUser] = useState({
        name: "",
        email: "",
        password: ""
    })
    const [passwordShown, setPasswordShown] = useState(false);

    const handleChange = event =>{
        const {name, value} = event.target;
        setUser({
            ...user,
            [name]: value
        })
    }

    const togglePasswordVisiblity = () => {
        setPasswordShown(passwordShown ? false : true);
    };

    const signUp = (e) =>{
        // e.preventDefault();
        const {name, email, password} = user;
        if(name && email && password){
            axios.post("/create-account", user)
            .then((res)=>{
                Swal2.fire({
                    icon : "success",
                    title : res.data.message
                })
                return res
            })
            .then((res)=>{
                localStorage.setItem("isAuthenticated", "true");
                localStorage.setItem("role", "User");
            })
            .then(()=>{
                navigate('/');
            })
            .catch((error)=>{
                Swal2.fire({
                    icon : "error",
                    title : error.response.data.message
                })
            })
        }else{
            Swal2.fire({
                icon : "error",
                title : "Invalid Inputs"
            })
        }

    }

    const validationSchema = Yup.object().shape({

        name: Yup.string()
            .required('Name is required'),
        email: Yup.string()
            .required('Email is required')
            .email('Email is invalid')
            .matches(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
            
        password: Yup.string()
            .required('Password is required')
            .min(6, 'Password must be at least 6 characters')
    });
    const formOptions = { resolver: yupResolver(validationSchema) };
    
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    return (
        <>
            <body class="hold-transition login-page">
            <div class="register-box">
            <div class="register-logo">
                <a href="../../index2.html" style={{ textDecoration: 'none' }} ><b>Movie Rental System</b></a>
            </div>

            <div class="card">
                <div class="card-body register-card-body">
                <p class="login-box-msg">Register a new membership</p>

                <form action="../../index.html" method="post">
                    <div class="input-group mb-3">
                        <input 
                            type="text" 
                            class="form-control" 
                            placeholder="Full name"
                            name="name" 
                            id="yourName" 
                            {...register('name')} 
                            className={`form-control ${errors.name ? 'is-invalid' : ''}`}
                            value = {user.name}
                            onChange = { handleChange }
                        />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-user"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">Please, enter your name!</div>
                    </div>

                    <div class="input-group mb-3">
                        <input 
                            type="email" 
                            class="form-control" 
                            placeholder="Email"
                            name="email" 
                            id="yourEmail" 
                            {...register('email')} 
                            className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                            value = {user.email}
                            onChange = { handleChange }
                        />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">Please enter a valid Email adddress!</div>
                    </div>

                    <div class="input-group mb-3">
                        <input 
                            class="form-control" 
                            placeholder="Password"
                            type={passwordShown ? "text" : "password"}
                            id="yourPassword" 
                            {...register('password')} 
                            className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                            value={user.password} 
                            onChange={handleChange}
                        />
                        <div class="input-group-append">
                            <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        <div class="invalid-feedback">Please enter your password!</div>
                    </div>

                    <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input 
                                type="checkbox" 
                                class="icheck-input" 
                                name="terms" 
                                value="" 
                                id="viewPass" 
                                onClick={togglePasswordVisiblity}
                            />
                            <label for="viewPass">
                            View Password
                            </label>
                        </div>
                    </div>
                    {/* <!-- /.col --> */}
                    <div class="col-4">
                        <button 
                            type="submit" 
                            class="btn btn-primary btn-block"
                            onClick ={handleSubmit(signUp)}
                            >Register
                        </button>
                    </div>
                    {/* <!-- /.col --> */}
                    </div>
                </form>

                <div class="social-auth-links text-center">
                    <p>- OR -</p>
                    {/* <a href="#" class="btn btn-block btn-primary">
                    <i class="fab fa-facebook mr-2"></i>
                    Sign up using Facebook
                    </a> */}
                    <a href="http://localhost:3040/auth/google" class="btn btn-block btn-danger">
                    <i class="fab fa-google-plus mr-2"></i>
                    Sign up using Google+
                    </a>
                </div>

                <a href="/login" class="text-center" style={{ textDecoration: 'none' }} >I already have a membership</a>
                </div>
                {/* <!-- /.form-box --> */}
            </div>
            {/* <!-- /.card --> */}
            </div>
            </body>
        </>
        
    )
};