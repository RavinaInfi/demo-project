import React, { useState, useEffect } from "react";
import './updateMovie.css';
import './style.css'
import axios from "axios";
import { useNavigate } from "react-router";
import Swal2 from "sweetalert2";
import { useLocation } from 'react-router-dom';


export default function UpdateMovie(){
    let navigate = useNavigate();
    const search = useLocation().search;
    const m_name = new URLSearchParams(search).get('name');
    const today = new Date().toISOString().split('T')[0];

    const data = new FormData();

    const getMovieDetails = async (name) => {
        try{
            const m_details = await axios.get(`/get-movie/${name}`);
            setMovie(m_details.data);

        }catch(err){
            Swal2.fire({
                icon : "error",
                title : "Something went wrong, check console"
            })
            console.log(err);
        }
    }

    useEffect(()=>{
        getMovieDetails(m_name)
    }, [])
    
    const [movie, setMovie] = useState({
        name: "",
        genre: "",
        releasDate: "",
        avalCD: 0,
        coins: 0,
        image: ""
    })

    let [{alt, src}, setImg] = useState({
        src: 'MovieImg',
        alt: 'Upload an Image'
    });

    const handleChange = event =>{
        const {name, value} = event.target;

        setMovie({
            ...movie,
            [name]: value
        })

        try{
            
            if(event.target.files[0]) {

                setMovie({
                    ...movie,
                    ["image"]: event.target.files[0],
                })

                
                setImg({
                    src: URL.createObjectURL(event.target.files[0]),
                    alt: event.target.files[0].name
                });
            }   
        }catch{
            setMovie({
                ...movie,
                [name]: value,
            })
        }
    }

    const updatingMovie = async () =>{
        const {name, genre, releasDate, avalCD, coins, image} = movie;
        if(image != ""){
            data.append('image', image);
            if(coins < 0){
                Swal2.fire({
                    icon : "error",
                    title : "Price can't be a negative number."
                })
            }
            else if(name && genre && releasDate && avalCD>=0 && coins){
                data.append('old_name', m_name);
                try{
                    var reg_name_lastname = /^[a-zA-Z\s]*$/;
    
                    if(!reg_name_lastname.test(movie.genre)){ 
                        Swal2.fire({
                            icon: "error",
                            title: "Correct Genre: only letters and spaces."
                        })
                    }else if(movie.releasDate > today){
                        Swal2.fire({
                            icon: "error",
                            title: "Invalid Date"
                        })
                    } else{
                        data.append('name', name);
                        data.append('genre', genre);
                        data.append('releasDate', releasDate);
                        data.append('avalCD', avalCD);
                        data.append('coins', coins);
                        let res = await axios.put("/update-movie", data);
                        await Swal2.fire({
                            icon : "success",
                            title : res.data.message
                        })
                        navigate('/admin-movie-page');
                    }
                }catch(error){
                    Swal2.fire({
                        icon : "error",
                        title : error.response.data.message
                    })
                    console.log(error.response.data);
                }
            }else{
                Swal2.fire({
                    icon : "error",
                    title : "Invalid Inputs"
                })
            }
        }else{
            data.append('image', movie.image);
            if(coins < 0){
                Swal2.fire({
                    icon : "error",
                    title : "Price can't be a negative number."
                })
            }
            else if(name && genre && releasDate && avalCD>=0 && coins){
                data.append('old_name', m_name);
                try{
                    var reg_name_lastname = /^[a-zA-Z\s]*$/;
    
                    if(!reg_name_lastname.test(movie.genre)){ 
                        Swal2.fire({
                            icon: "error",
                            title: "Correct Genre: only letters and spaces."
                        })
                    }else if(movie.releasDate > today){
                        Swal2.fire({
                            icon: "error",
                            title: "Invalid Date"
                        })
                    } else{
                        data.append('name', name);
                        data.append('genre', genre);
                        data.append('releasDate', releasDate);
                        data.append('avalCD', avalCD);
                        data.append('coins', coins);
                        let res = await axios.put("/update-movie", data);
                        await Swal2.fire({
                            icon : "success",
                            title : res.data.message
                        })
                        navigate('/admin-movie-page');
                    }
                }catch(error){
                    Swal2.fire({
                        icon : "error",
                        title : error.response.data.message
                    })
                    console.log(error.response.data);
                }
            }else{
                Swal2.fire({
                    icon : "error",
                    title : "Invalid Inputs"
                })
            }
        }

    }

    if (movie.image) {
        if(typeof(movie.image) === "string"){
            src = "http://localhost:3040/"+movie.image;
        }
    }else{
        src = "dist/img/flower.jpeg"
    }

    return (
        <>
            <div class="content-header">
                <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                    <h1 class="m-0">Edit Movie</h1>
                    </div>
                    <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="/admin-movie-page">Movies</a></li>
                        <li class="breadcrumb-item active">Update</li>
                    </ol>
                    </div>
                </div>
            
            <div align="center">
            <div className="updateMovie">
                <h1>Movie Details</h1>

                <details>
                    <summary>{m_name}</summary>
                    <p>The Old Movie Name</p>
                </details>

                <div className="form__img-input-container">
                    <input 
                        type="file" 
                        accept=".png, .jpg, .jpeg" 
                        id="photo"
                        onChange={handleChange}

                    />
                    <img src={src} alt={alt} className="form-img__img-preview"/>
                </div>

                <input 
                    type="text" 
                    name = "name" 
                    value = {movie.name} 
                    placeholder="Movie Name" 
                    onChange = { handleChange }
                    ></input>
                <input 
                    type="text" 
                    name = "genre" 
                    value = {movie.genre} 
                    placeholder="Genre" 
                    onChange = { handleChange }
                    ></input>
                <input 
                    type="date" 
                    max = {today} 
                    name = "releasDate" 
                    value = {movie.releasDate.substring(0, 10)} 
                    placeholder="Release Date" 
                    onChange = { handleChange }
                    ></input>
                <input 
                    type="number" 
                    name = "avalCD" 
                    value = {Number(movie.avalCD)} 
                    placeholder="rents" 
                    onChange = { handleChange }
                    ></input>
                <input 
                    type="number" 
                    name = "coins" 
                    value = {Number(movie.coins)} 
                    placeholder="price" 
                    onChange = { handleChange }
                    ></input>

                <button 
                    className="button" 
                    onClick = {updatingMovie}
                    >Update
                </button>
            </div>
            </div>
            </div>
            </div>
        </>
    )
};