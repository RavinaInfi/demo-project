import React, {useState, useEffect} from "react";
import axios from "axios";
import './rents.css';
import { useNavigate } from "react-router";
import { useLocation } from 'react-router-dom';

export default function MovieRentsDetails(){
    const navigate = useNavigate()
    const search = useLocation().search;
    const name = new URLSearchParams(search).get('name');

    let [rents, setRentDetails] = useState([]);

    useEffect(()=>{
        axios.get(`/movie-rents/${name}`)
        .then((response) => {
            setRentDetails(response.data);
        })
        .catch((err) =>{
            try{
                console.log(err.response);
                if(err.response.data.message === "No Rents"){
                    setRentDetails(["No Rents"]);
                }
            }catch(err){
                console.log(err);
            }
        })
    }, [])

    return (
        <>
        <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0">Rents</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/admin-movie-page">Movies</a></li>
                    <li class="breadcrumb-item active">Rent Details</li>
                  </ol>
                </div>
              </div>
        
                <div className="row">
                {rents.map((movie) =>{
                    if(movie === "No Rents"){
                        return (
                            <p className="text-center text-danger">No Rents</p>
                        )
                    }else{
                        return(
                            <div className="col-md-3" key={movie._id}>
                                <div className ="card">
                                    <div className="card-header text-center card-header-color" >
                                        <h5 className="movie-title" >{movie.user_id.name}</h5>
                                    </div>
                                    <div className="card-body">
                                        <p className="email">Email: {movie.user_id.email}</p>
                                        <p className ="role">Role: {movie.user_id.role}</p>
                                        <p className ="rents">Total rents: {movie.user_id.rent}</p>
                                    </div>
                                </div>
                            </div>
                        )
                    }})}
            </div>
            </div>
            </div>
        </>
    )
}