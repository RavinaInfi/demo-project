import React, {useState, useEffect} from "react";
import axios from "axios";
import './profile.css';
import { useNavigate } from "react-router";
import { useCookies } from "react-cookie";
import Swal2 from "sweetalert2";
import EditProfile from '../editProfile/editProfile';

let check = false;
export default function UserProfile(){
    const navigate = useNavigate()

    let [userDetails, setUserDetails] = useState([]);
    const [rents, setRents] = useState(0);
    let [cookies, setCookie] = useCookies(['token']);
    const [state, setState] = useState({});
    setCookie('token', cookies);

    const profileDetails = () => {
        axios.get('/view-profile', {headers: {cookie: cookies}})
        .then((response) => {
            if(response.data === "no_user"){
                setUserDetails(["no_user"]);
            }else{
                setUserDetails(response.data.view_profile[0][0]);
                setRents(response.data.view_profile[1])
            }
        })
        .catch((err) =>{
            try{
                if(err.data === "no_user"){
                    setUserDetails(["no_user"]);
                }
            }catch(err){
                console.log(err);
            }
        })
    }

    useEffect(()=>{
        profileDetails();
        return () =>{
            return setState({});
        }
    }, [])

    const total_rents = (rents) =>{
        if(rents>0){
            navigate('/total-rents', { state: { user: {email: userDetails.email, id: userDetails._id} } });
            return;
        }
        Swal2.fire({
            icon: "error",
            title: "No Rents Available."
        })
        .then(()=>{
            profileDetails();
        })
    }

    const userInformation = () =>{
        if(userDetails[0] === "no_user"){
            return (
            <div class="row">
                <div className ="card border-warning">
                    <div className="card-body">
                        <p className="text-center text-danger">You are not logged in.</p>
                    </div>
                </div>
            </div>
            )
        }else{
            return (
            <div className="user">
                <div class="row">
                    <div class="col-xl-3">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="../../dist/img/profile.png"
                                alt="User profile picture"
                            />
                        </div>
                        <h3 class="profile-username text-center">{userDetails.name}</h3>
                        <p class="text-muted text-center">{userDetails.role}</p>
                        <button 
                            id="rents" 
                            type="submit" 
                            class="btn btn-primary btn-block"
                            onClick={()=>{
                                total_rents(rents)
                            }}
                            >View Total Rents
                        </button>
                        </div>
                    </div>

                    <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-4 label ">Full Name</div>
                            <div class="col-lg-8 text-muted">{userDetails.name}</div>
                        </div>
                        <hr></hr>
                        <div class="row">
                            <div class="col-lg-4 label">Country</div>
                            <div class="col-lg-8 text-muted">India</div>
                        </div>
                        <hr></hr>
                        <div class="row">
                            <div class="col-lg-4 label">Email</div>
                            <div class="col-lg-8 text-muted">{userDetails.email}</div>
                        </div>
                        <hr></hr>
                        <div class="row">
                            <div class="col-lg-4 label">Wallet(Rs)</div>
                            <div class="col-lg-8 text-muted">{userDetails.coins} Rs.</div>
                        </div>
                        <hr></hr>
                        <div class="row">
                            <div class="col-lg-4 label">Total Rents</div>
                            <div class="col-lg-8 text-muted">{rents}</div>
                        </div>
                        <hr></hr>
                        <p class="text-muted text-center">***</p>
                        <a href="/logout" class="btn btn-primary btn-block">Logout</a>
                    </div>
                    </div>

                    </div>

                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header p-2">
                            {/* <!-- Bordered Tabs --> */}
                                <ul class="nav nav-pills">

                                    <li class="nav-item">
                                    <a class="nav-link active" href="#profile-overview" data-toggle="tab">Overview</a>
                                    </li>

                                    <li class="nav-item">
                                    <a class="nav-link" href="#profile-edit" data-toggle="tab">Edit Profile</a>
                                    </li>

                                </ul>
                            </div>

                            <div class="card-body">
                            
                                <div class="tab-content">

                                    <div class="active tab-pane" id="profile-overview">
                                    <div class="post">
                                        <div class="user-block">
                                            {/* <img class="img-circle img-bordered-sm" src="../../dist/img/user6-128x128.jpg" alt="User Image"/> */}
                                            {/* <span class="username"> */}
                                            <h3>Pictures of the day</h3>
                                            {/* <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> */}
                                            {/* </span> */}
                                            <p>Treating the other person as you would like to be treated is a golden rule that not 
                                                only applies to general life encounters but also in negotiations.</p>
                                            <p>A wise person once said, "Life is not happening to you. Life is responding to you." 
                                                - author unknown. This is absolutely true. You dictate what life hands you. 
                                                If you make up your mind to be successful, odds are, you will be.</p>
                                        </div>
                                        {/* <!-- /.user-block --> */}
                                        <div class="row mb-3">
                                            <div class="col-sm-6">
                                            <img class="img-fluid" src="../../dist/img/photo1.png" alt="Photo"/>
                                            </div>
                                            {/* <!-- /.col --> */}
                                            <div class="col-sm-6">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                <img class="img-fluid mb-3" src="../../dist/img/photo2.png" alt="Photo"/>
                                                <img class="img-fluid" src="../../dist/img/photo3.jpg" alt="Photo"/>
                                                </div>
                                                {/* <!-- /.col --> */}
                                                <div class="col-sm-6">
                                                <img class="img-fluid mb-3" src="../../dist/img/photo4.jpg" alt="Photo"/>
                                                <img class="img-fluid" src="../../dist/img/photo1.png" alt="Photo"/>
                                                </div>
                                                {/* <!-- /.col --> */}
                                            </div>
                                            {/* <!-- /.row --> */}
                                            </div>
                                            {/* <!-- /.col --> */}
                                        </div>
                                        {/* <!-- /.row --> */}

                                        <p>
                                            {/* <a href="#" class="link-black text-sm mr-2"><i class="fas fa-share mr-1"></i> Share</a>
                                            <a href="#" class="link-black text-sm"><i class="far fa-thumbs-up mr-1"></i> Download</a> */}
                                            <span class="float-right">
                                            <a href="#" class="link-black text-sm">
                                                <i class="far fa-comments mr-1"></i> Comments (5)
                                            </a>
                                            </span>
                                        </p>

                                        <input class="form-control form-control-sm" type="text" placeholder="Type a comment"/>
                                    </div>
                                    {/* </div> */}
                                    </div>

                                    <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                                    {/* <!-- Profile Edit Form --> */}
                                    < EditProfile UserData = {{data: userDetails}} />
                                    {/* <!-- End Profile Edit Form --> */}

                                    </div>

                                </div>
                                {/* <!-- End Bordered Tabs --> */}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            )
        }
    }

    return (
        <>
            <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0">About</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                  </ol>
                </div>
              </div>
            </div>
            </section>
            <section class="content">
            <div class="container-fluid">
            {userInformation()}
        </div>
        </section>
        </>
    )
}