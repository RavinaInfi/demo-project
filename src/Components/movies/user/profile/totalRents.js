import React, { useState, useEffect } from "react";
import './profile.css';
import { useNavigate } from "react-router";
import * as moment from "moment";
import axios from "axios";
import Swal2 from "sweetalert2";
import ReactPaginate from "react-paginate";
import { useCookies } from "react-cookie";
import { useLocation } from "react-router-dom";


export default function TotalRents(){
    const navigate = useNavigate()
    const { user } = useLocation().state;

    let [cookies, setCookie] = useCookies(['token']);
    setCookie('token', cookies);

    const [movies, setMovies] = useState([]);
    const [state, setState] = useState({});
    const [searchTerm, setSearchTerm] = useState("");
    const [totalNumMovies, setTotalNumMovies] = useState(1);
    const myPageNumber = 1

    const [dates, setDates] = useState({
        fromDate: "",
        toDate: ""
    });

    const moviesPerPage = 6;
    const pageCount = Math.ceil(totalNumMovies / moviesPerPage);
    const today = new Date().toISOString().split('T')[0];

    const changePage = ({ selected }) => {
        return rents(selected+1);
    }

    const handleDate = (e) =>{
        const {name, value} = e.target;
        setDates({
            ...dates,
            [name]: value
        })
    };

    const rents = (myPageNumber=1) =>{
        axios.get(`/total-rents?_pageNum=${myPageNumber}&_limit=${moviesPerPage}&_toDate=${dates.toDate}&_fromDate=${dates.fromDate}&_search=${searchTerm}`, {headers: {cookie: cookies}})   
        .then((response) => {
            setTotalNumMovies(response.data.total_movies)
            return setMovies(response.data.movies);
        })
        .catch((err)=>{
            console.log(err);
            alert(err.response)
        })
    }

    useEffect(()=>{
        rents();
        return () =>{
            return setState({});
        }
    }, []);

    const sortByReleasDate = () =>{
        const { toDate, fromDate } = dates;
        if(toDate && fromDate){
            axios.get(`/total-rents?_pageNum=${myPageNumber}&_limit=${moviesPerPage}&_toDate=${toDate}&_fromDate=${fromDate}&_search=${searchTerm}`, {headers: {cookie: cookies}})
            .then((response) =>{
                setTotalNumMovies(response.data.total_movies)
                return setMovies(response.data.movies);
            })
            .catch((err) =>{ 
                Swal2.fire({
                    icon : "error",
                    title : err.response.data
                })
            })
        }else{
            Swal2.fire({
                icon : "error",
                title : "Please ensure you pick two dates"
            })
        }
    }

    const searchMovie = (value) =>{
        axios.get(`/total-rents?_pageNum=${myPageNumber}&_limit=${moviesPerPage}&_toDate=${dates.toDate}&_fromDate=${dates.fromDate}&_search=${value}`, {headers: {cookie: cookies}})
        .then((response) =>{
            console.log(response.data.movies)
            setTotalNumMovies(response.data.total_movies)
            return setMovies(response.data.movies);
        })
        .catch((err) =>{ 
            Swal2.fire({
                icon : "error",
                title : err.response.data
            })
        })
    }

    function confirmation() {
        let person = prompt(`Are you sure to return the movie?, won't be back once returned`, "yes");
        
        if (person === null) {
            return 'no'
        }
        else if (person.toLowerCase() === 'no') {
            return 'no'
        } else if(person.toLowerCase() === 'yes'){
            return 'yes'
        }
         else {
            return 'inValid';
        }
      }

    const returnMovie = async (movie) =>{
        
        const isConfirm = confirmation();
        if (isConfirm === 'no') {
            return false;
        }else if (isConfirm === 'inValid') {
            Swal2.fire({
                icon: "error",
                title: `please type yes/no, wrong input.`
            })
        }else{

            const re_mov_details = {
                name: movie.name,
                rentDate: movie.rentDate,
                releasDate: movie.releasDate,
                returnDate: today,
                genre: movie.genre,
                user: user.email,
                user_id: user.id,
            }
    
            console.log("re_mov_details", re_mov_details);

            function addAmn(currentAmn, additionalAmn) {
                let user_input = (prompt(`Add amount min(${additionalAmn-currentAmn}) Rs.`, additionalAmn-currentAmn));
                if (user_input === null) {
                    return 'inValid'
                }

                let person = parseInt(user_input);

                if (isNaN(person)) {
                    return 'inValid'
                } else if(person<0){
                    return 'inValid'
                }
                 else {
                     return person;
                }
              }
    
            try{
                const return_movie = await axios.post('/return-movie', re_mov_details);
                if(return_movie.data.message === 'addAmount'){
                    
                    let wallet_info = return_movie.data;
    
                    const amount = addAmn(wallet_info.wallet, wallet_info.additional_amount);
                    console.log("amount", amount);
    
                    if (amount === 'inValid') {
                        Swal2.fire({
                            icon: "error",
                            title: `Wrong Input.`
                        })
                    }else{
                        axios.put(`/add-to-wallet?amount=${amount}&user=${user.email}`)
                        .then(()=>{
                            Swal2.fire({
                                icon: "success",
                                text: `amount added ${amount}`
                            }) 
                        })
                    }
                }else if(return_movie.data.message === "success"){
                    Swal2.fire({
                        icon: "success",
                        title: "Thank you ❤️",
                        text: `delayed fee: ${return_movie.data.deducted_amount}`
    
                    })
                    .then(()=>{
                        navigate('/profile');
                    })
    
                }else if(return_movie.data.message === "failed"){
                    await Swal2.fire({
                        icon: "success",
                        title: "Thank you ❤️",
                        footer: '<a href="#">FeedBack</a>'
                    })
                    .then(()=>{
                        navigate('/profile');
                    })
                }else{
                    console.log("return_movie.data", return_movie.data);
                }
            }catch(err){
                console.log(err);
            }
        }
    }

    return (
        <>
            <div class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1 class="m-0">Total Rents</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="/profile">Profile</a></li>
                    <li class="breadcrumb-item active">Total Rents</li>
                  </ol>
                </div>
              </div>

              <input 
                type="date" 
                date = "fromDate" 
                name="fromDate"
                max = {today}
                value = {dates.fromDate} 
                placeholder="To Date" 
                onChange = { handleDate }
            ></input>
            <input 
                type="date" 
                name="toDate"
                max = {today}
                value = {dates.toDate} 
                placeholder="From Date" 
                style={{'marginRight': '5px'}}
                onChange = { handleDate }
            ></input>
            <button 
                className="btn btn-primary" 
                id="sortByReleasDate" 
                style={{'marginRight': '65px'}} 
                onClick={() => {sortByReleasDate()}}
                >Filter
            </button>
            <input 
                type="text" 
                id="search" 
                style={{'marginLeft': '55%'}} 
                placeholder="Search...."
                onChange={(event) => {
                    setSearchTerm(event.target.value);
                    searchMovie(event.target.value);
                }} 
            /> 
            
            <div className="row">
            {movies.map((movie) =>{
                if (movie.image) {
                    var image = "http://localhost:3040/"+movie.image;
                }else{
                    var image = "dist/img/beach.jpeg"
                }
                return (
                    <div className="col-md-4" key={movie._id}>
                        <div className ="card">
                        <div className="card-header card-header-color" >
                            <h5 className="card-title" style={{'marginLeft': '20px'}} >{movie.name}</h5>
                        </div>
                            <img src={image} class="card-img-top" alt="..."/>
                            <div className="card-body">
                                <p className="genre">Genre: {movie.genre}</p>
                                <p className ="releasDate">Releas Date: {moment(movie.releasDate).format('DD/MM/YYYY')}</p>
                                <p className="rentDate">Rent Date: {moment(movie.rentDate).format('DD/MM/YYYY')}</p>
                                <button className="btn btn-primary" onClick={()=>{returnMovie(movie)}}>Return</button>
                            </div>
                        </div>
                    </div>
                )}
            )}
            </div>

            <div className="row">
                    <ReactPaginate
                        previousLabel = {"<<"}
                        nextLabel = {">>"}
                        breakLabel={'...'}
                        pageCount={pageCount}
                        marginPagesDisplayed={4}
                        pageRangeDisplayed={4}
                        onPageChange={changePage}
                        containerClassName={"pagination justify-content-center"}
                        pageClassName={"page-item"}
                        pageLinkClassName={"page-link"}
                        previousClassName={"page-item"}
                        previousLinkClassName={"page-link"}
                        nextClassName={"page-item"}
                        nextLinkClassName={"page-link"}
                        breakClassName={"page-item"}
                        breakLinkClassName={"page-link"}
                        activeClassName={"active"}
                    />
                </div>
            </div>
            </div>
        </>
    )
}