import React, { useState, useEffect } from "react";
import './editProfile.css';
import axios from "axios";
import { useNavigate } from "react-router";
import Swal2 from "sweetalert2";


export default function EditProfile(props){
    let navigate = useNavigate();

    const [user, setUser] = useState({})

    useEffect(()=>{
        const userInfo =  props.UserData.data;
        setUser(userInfo)
    }, [props.UserData.data])
    
    const handleChange = event =>{
        const {name, value} = event.target;
        setUser({
            ...user,
            [name]: value
        })
    }

    const updatingUser = () =>{
        const {name, email, role, coins} = user;
        if(name && email && role && coins>0){
            user["id"] = props.UserData.data._id;
            try{
                var reg_name_lastname = /^[a-zA-Z\s]*$/;

                if(!reg_name_lastname.test(name)){ 
                    Swal2.fire({
                        icon: "error",
                        title: "Correct Genre: only letters and spaces."
                    })
                }else{
                    axios.put(`/edit-profie?id=${props.UserData.data._id}`, user)
                    .then((res)=>{
                        Swal2.fire({
                            icon : "success",
                            title : res.data
                        })
                        return res;
                    })
                    .then(()=>{
                        navigate('/profile');
                    })
                    .catch((err)=>{
                        console.log(err);
                    })
                }
            }catch(error){
                Swal2.fire({
                    icon : "error",
                    title : error.response.data.message
                })
                console.log(error.response.data);
            }
        }else{
            if(coins <= 0){
                Swal2.fire({
                    icon : "error",
                    title : "Valet can't be zero or less then zero."
                })
                .then(()=>{
                    handleChange()
                })
            }else{
                Swal2.fire({
                    icon : "error",
                    title : "Invalid Inputs"
                })
            }
        }

    }

    return (
        <>
            <form>
                <div class="row mb-3">
                <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                <div class="col-md-8 col-lg-9">
                    <img class="profile-user-img img-fluid img-circle"
                        src="../../dist/img/profile.png" 
                        alt="Profile"
                    />
                </div>
                </div>

                <div class="row mb-3">
                <label htmlFor="name" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                <div class="col-md-8 col-lg-9">
                    <input 
                        type="text" 
                        name="name" 
                        value= {user.name} 
                        class="form-control" 
                        id="fullName" 
                        placeholder="Name" 
                        onChange = { handleChange }
                        >
                    </input>
                </div>
                </div>

                <div class="row mb-3">
                <label htmlFor="Country" class="col-md-4 col-lg-3 col-form-label">Role</label>
                <div class="col-md-8 col-lg-9">
                    <input 
                        type="text" 
                        name="role" 
                        value={user.role} 
                        class="form-control" 
                        placeholder="Role"
                        id="Country" 
                        disabled 
                        onChange = { handleChange }
                    />
                </div>
                </div>


                <div class="row mb-3">
                <label htmlFor="Email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                <div class="col-md-8 col-lg-9">
                    <input 
                        type="email" 
                        name="email" 
                        value={user.email} 
                        class="form-control" 
                        id="Email" 
                        placeholder="Email"
                        onChange = { handleChange }
                    />
                </div>
                </div>

                <div class="row mb-3">
                <label htmlFor="coins" class="col-md-4 col-lg-3 col-form-label">Wallet(Rs)</label>
                <div class="col-md-8 col-lg-9">
                    <input 
                        type="number" 
                        name="coins" 
                        value={Number(user.coins)} 
                        class="form-control" 
                        placeholder="Wallet"
                        id="coins" 
                        onChange = { handleChange }
                    />
                </div>
                </div>
                
                <div class="text-center">
                <button 
                    type="submit" 
                    class="btn btn-primary" 
                    onClick = {updatingUser}
                    >Save Changes
                </button>
                </div>
            </form> 
            
        </>
    )
};
