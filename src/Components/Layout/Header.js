import React, { useState } from 'react'

export default function Header(){

  const isAuthenticated = localStorage.getItem("isAuthenticated");
  const role = localStorage.getItem('role');

  if(role != null && isAuthenticated != null){
    if (isAuthenticated && role.toLowerCase() === 'admin') {
      return(
        <div>
        {/* <!-- Navbar --> */}
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
          {/* <!-- Left navbar links --> */}
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
              <a href="/" class="nav-link">Home</a>
            </li>
          </ul>

          {/* <!-- Right navbar links --> */}
          <ul class="navbar-nav ml-auto">
            {/* <!-- Navbar Search --> */}
            <li class="nav-item">
              <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                <i class="fas fa-search"></i>
              </a>
              <div class="navbar-search-block" id="main-header-search">
                <form class="form-inline">
                  <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                      </button>
                      <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </nav>
        {/* <!-- /.navbar --> */}
        
        {/* <!-- Main Sidebar Container --> */}
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
          {/* <!-- Brand Logo --> */}
          <a href="" class="brand-link" style={{ 'textDecoration': 'none' }}>
            <img src='../../../dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
            <span class="brand-text font-weight-light">Movie Rental Sys</span>
          </a> 

          {/* <!-- Sidebar --> */}
          <div class="sidebar">
             {/* <!-- Sidebar user (optional) -->  */}
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                <img src="dist/img/profile.png" class="img-circle elevation-2" alt="User Image"/>
              </div>
              <div class="info">
                <a href="/profile" class="d-block" style={{ 'textDecoration': 'none' }}>Profile</a>
              </div>
            </div>

            {/* <!-- SidebarSearch Form --> */}
            <div class="form-inline">
              <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                <div class="input-group-append">
                  <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                  </button>
                </div>
              </div>
            </div>

            {/* <!-- Sidebar Menu --> */}
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                {/* Dashboard */}
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Dashboard
                    </p>
                  </a>
                </li>

                {/* Movies */}
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                      Movies
                      <i class="fas fa-angle-left right"></i>
                      <span class="badge badge-info right"></span>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="/movies-add-movie" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add Movie</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/admin-movie-page" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Admin Movie Page</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/user-movie-page" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>User Movie Page</p>
                      </a>
                    </li>
                  </ul>
                </li>

                {/* Users */}
                <li class="nav-item">
                  <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                      Users
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview">
                    <li class="nav-item">
                      <a href="/users" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>All users</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="/add-user" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Add user</p>
                      </a>
                    </li>
                  </ul>
                </li>

                {/* Lables */}
                <li class="nav-header">LABELS</li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-danger"></i>
                    <p class="text">Important</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-warning"></i>
                    <p>Warning</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-info"></i>
                    <p>Informational</p>
                  </a>
                </li>
                <li class="nav-item">
                    <a href="/logout" class="nav-link">
                      <i class="nav-icon far fa-circle text-info"></i>
                      <p>Logout</p>
                    </a>
                  </li>
              </ul>
            </nav>
            {/* <!-- /.sidebar-menu --> */}
          </div>
          {/* <!-- /.sidebar --> */}
        </aside> 
        </div>
      )
    }else{
      return(
        <div>
          {/* <!-- Navbar --> */}
          <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            {/* <!-- Left navbar links --> */}
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
              </li>
              <li class="nav-item d-none d-sm-inline-block">
                <a href="/" class="nav-link">Home</a>
              </li>
            </ul>

            {/* <!-- Right navbar links --> */}
            <ul class="navbar-nav ml-auto">
              {/* <!-- Navbar Search --> */}
              <li class="nav-item">
                <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                  <i class="fas fa-search"></i>
                </a>
                <div class="navbar-search-block" id="main-header-search">
                  <form class="form-inline">
                    <div class="input-group input-group-sm">
                      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                      <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                          <i class="fas fa-search"></i>
                        </button>
                        <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                          <i class="fas fa-times"></i>
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </li>

              <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                  <i class="fas fa-expand-arrows-alt"></i>
                </a>
              </li>
            </ul>
          </nav>
          {/* <!-- /.navbar --> */}
          
          {/* <!-- Main Sidebar Container --> */}
          <aside class="main-sidebar sidebar-dark-primary elevation-4">
            {/* <!-- Brand Logo --> */}
            <a href="" class="brand-link" style={{ 'textDecoration': 'none' }}>
              <img src='../../../dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
              <span class="brand-text font-weight-light">Movie Rental Sys</span>
            </a> 

            {/* <!-- Sidebar --> */}
            <div class="sidebar">
              {/* <!-- Sidebar user (optional) -->  */}
              <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                  <img src="dist/img/profile.png" class="img-circle elevation-2" alt="User Image"/>
                </div>
                <div class="info">
                  <a href="/profile" class="d-block" style={{ 'textDecoration': 'none' }}>Profile</a>
                </div>
              </div>

              {/* <!-- SidebarSearch Form --> */}
              <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                  <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                  <div class="input-group-append">
                    <button class="btn btn-sidebar">
                      <i class="fas fa-search fa-fw"></i>
                    </button>
                  </div>
                </div>
              </div>

              {/* <!-- Sidebar Menu --> */}
              <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                  {/* Dashboard */}
                  <li class="nav-item">
                    <a href="/" class="nav-link">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Dashboard
                      </p>
                    </a>
                  </li>

                  {/* Movies */}
                  <li class="nav-item">
                    <a href="/user-movie-page" class="nav-link">
                      <i class="nav-icon fas fa-copy"></i>
                      <p>
                        Movies
                        {/* <i class="fas fa-angle-left right"></i> */}
                        <span class="badge badge-info right"></span>
                      </p>
                    </a>
                  </li>
                  {/* Lables */}
                  <li class="nav-header">LABELS</li>
                  <li class="nav-item">
                    <a href="/" class="nav-link">
                      <i class="nav-icon far fa-circle text-danger"></i>
                      <p class="text">Important</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/" class="nav-link">
                      <i class="nav-icon far fa-circle text-warning"></i>
                      <p>Warning</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/" class="nav-link">
                      <i class="nav-icon far fa-circle text-info"></i>
                      <p>Informational</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/logout" class="nav-link">
                      <i class="nav-icon far fa-circle text-info"></i>
                      <p>Logout</p>
                    </a>
                  </li>
                </ul>
              </nav>
              {/* <!-- /.sidebar-menu --> */}
            </div>
            {/* <!-- /.sidebar --> */}
          </aside> 
        </div>
      )
    }
  }else{
    return(
      <div>
        {/* <!-- Navbar --> */}
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
          {/* <!-- Left navbar links --> */}
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
              <a href="/" class="nav-link">Home</a>
            </li>
          </ul>

          {/* <!-- Right navbar links --> */}
          <ul class="navbar-nav ml-auto">
            {/* <!-- Navbar Search --> */}
            <li class="nav-item">
              <a class="nav-link" data-widget="navbar-search" data-target="#main-header-search" href="#" role="button">
                <i class="fas fa-search"></i>
              </a>
              <div class="navbar-search-block" id="main-header-search">
                <form class="form-inline">
                  <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search"/>
                    <div class="input-group-append">
                      <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search"></i>
                      </button>
                      <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                        <i class="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
              </a>
            </li>
          </ul>
        </nav>
        {/* <!-- /.navbar --> */}
        
        {/* <!-- Main Sidebar Container --> */}
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
          {/* <!-- Brand Logo --> */}
          <a href="" class="brand-link" style={{ 'textDecoration': 'none' }}>
            <img src='../../../dist/img/movieIcon5.png' alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style={{"opacity": .8}}/>
            <span class="brand-text font-weight-light">Movie Rental Sys</span>
          </a> 

          {/* <!-- Sidebar --> */}
          <div class="sidebar">
             {/* <!-- Sidebar user (optional) -->  */}
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                <img src="dist/img/login.jpeg" class="img-circle elevation-2" alt="User Image"/>
              </div>
              <div class="info">
                <a href="/login" class="d-block" style={{ 'textDecoration': 'none' }}>Login</a>
              </div>
            </div>
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                <img src="dist/img/signUp.png" class="img-circle elevation-2" alt="User Image"/>
              </div>
              <div class="info">
                <a href="/create-account" class="d-block" style={{ 'textDecoration': 'none' }}>signUp</a>
              </div>
            </div>

            {/* <!-- SidebarSearch Form --> */}
            <div class="form-inline">
              <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search"/>
                <div class="input-group-append">
                  <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                  </button>
                </div>
              </div>
            </div>

            {/* <!-- Sidebar Menu --> */}
            <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                {/* Dashboard */}
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                      Dashboard
                    </p>
                  </a>
                </li>

                {/* Movies */}
                <li class="nav-item">
                  <a href="/user-movie-page" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                      Movies
                      {/* <i class="fas fa-angle-left right"></i> */}
                      <span class="badge badge-info right"></span>
                    </p>
                  </a>
                </li>

                {/* Lables */}
                <li class="nav-header">LABELS</li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-danger"></i>
                    <p class="text">Important</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-warning"></i>
                    <p>Warning</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/" class="nav-link">
                    <i class="nav-icon far fa-circle text-info"></i>
                    <p>Informational</p>
                  </a>
                </li>
              </ul>
            </nav>
            {/* <!-- /.sidebar-menu --> */}
          </div>
          {/* <!-- /.sidebar --> */}
        </aside> 
      </div>
    )
  }
}
